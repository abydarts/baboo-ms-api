<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Trans extends REST_Controller
{
    // constructor for the class
    public function __construct(){
        parent::__construct();
        $this->load->model('Trans_model');
        $this->load->library('Php_func');
    }

    // default GET method
    public function index_get(){
        $id = $this->uri->segment(4);

         if ($id !== NULL) {
            $order = $this->Trans_model->getTrans($id);
        } else{
            $order = $this->Trans_model->getTrans();
        }

        if ($order) {
            return $this->response(
                [
                    'success' => TRUE,
                    'data' => $order
                ],
                REST_Controller::HTTP_OK,
                TRUE
            );
        }

        return $this->response(
            [
                'success' => FALSE,
                'message' => 'transaction not found'
            ],
            REST_Controller::HTTP_NOT_FOUND,
            TRUE
        );
    }

    // default POST method
    public function index_post(){
        $book_id = $this->post('book_id') ?? NULL;
        $price = $this->post('price') ?? NULL;

        $data = 
        [
            'book_id' => $book_id,
            'price' => $price
        ];

        if ($this->Trans_model->createTrans($data) > 0) {
            return $this->response(
                [
                    'success' => TRUE,
                    'message' => 'new transaction has been placed',
                    'data' => $data
                ],
                REST_Controller::HTTP_OK,
                TRUE
            );
        }

        return $this->response(
            [
                'success' => FALSE,
                'message' => 'failed to place transaction'
            ],
            REST_Controller::HTTP_BAD_REQUEST,
            TRUE
        );

    }

    // default PUT method
    public function index_put(){
        $id = $this->uri->segment(4);

        if($id === NULL){
            return $this->response(
                [
                    'success' => FALSE,
                    'message' => 'provide an id'
                ],
                REST_Controller::HTTP_BAD_REQUEST,
                TRUE
            );
        }

        $book_id = $this->put('book_id') ?? NULL;
        $price = $this->put('price') ?? NULL;
        
        $data = ['id' => $id ];

        if ($book_id !== NULL) {
            $data += ['book_id' => $book_id];
        
        }

        if ($price !== NULL) {
            $data += ['price' => $price];
        
        }

        if ($this->Trans_model->updateTrans($data, $id) > 0) {
            return $this->response(
                [
                    'success' => TRUE,
                    'message' => 'transaction has been updated',
                    'data' => $data
                ],
                REST_Controller::HTTP_OK,
                TRUE
            );
        }

        return $this->response(
            [
                'success' => FALSE,
                'message' => 'failed to update order'
            ],
            REST_Controller::HTTP_BAD_REQUEST,
            TRUE
        );

    }

    // default DELETE method
    public function index_delete(){
        $id = $this->uri->segment(4);

        if($id === NULL){
            return $this->response(
                [
                    'success' => FALSE,
                    'message' => 'provide an id'
                ],
                REST_Controller::HTTP_BAD_REQUEST,
                TRUE
            );
        }

        if ($this->Trans_model->deleteTrans($id) > 0) {
            return $this->response(
                [
                    'success' => TRUE,
                    'message' => 'transaction has been deleted'
                ],
                REST_Controller::HTTP_OK,
                TRUE
            );
        }

        return $this->response(
            [
                'success' => FALSE,
                'message' => 'failed to delete transaction'
            ],
            REST_Controller::HTTP_BAD_REQUEST,
            TRUE
        );

    }

    // Checkout POST method
    public function checkout_post(){
        $id = $this->uri->segment(5);

        if ($id === NULL) {
            return $this->response(
                [
                    'success' => FALSE,
                    'message' => 'provide an id'
                ],
                REST_Controller::HTTP_BAD_REQUEST,
                TRUE
            );
        }

        $data = $this->Trans_model->getTrans($id);

        if (empty($data)){
            return $this->response(
                [
                    'success' => FALSE,
                    'message' => 'order not found'
                ],
                REST_Controller::HTTP_NOT_FOUND,
                TRUE
            );
        }

        $data = json_encode($data);
        $order = $this->php_func->processCheckout($data);
        if (!empty($order)){

            $result = json_decode($order, true);

            return $this->response(
                [
                    'success' => TRUE,
                    'message' => 'order has been checkout successfully',
                    'data' => $result
                ],
                REST_Controller::HTTP_OK,
                TRUE
            );
        }

        return $this->response(
            [
                'success' => FALSE,
                'message' => 'cannot process checkout'
            ],
            REST_Controller::HTTP_BAD_REQUEST,
            TRUE
        );
    }
}