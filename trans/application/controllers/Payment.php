<?php
defined("BASEPATH") OR exit("No direct script access allowed");
// This can be removed if you use __autoload() in config.php OR use Modular Extensions

class Payment extends REST_Controller
{

    public function __construct()
    {
        //code here
        parent::__construct();
        $this->load->helper("baboo");
    }

    public function index_post()
    {
        $data_post = $this->post();
        $this->pay_post($data_post);
    }

    public function pay_post($data_post)
    {
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        if(!empty($data_post)){
            $data_insert = array();
            if(isset($data_post["transaction_id"])) $data_insert["transaction_id"]         = $data_post["transaction_id"];
            if(isset($data_post["order_id"])) $data_insert["order_id"]                     = $data_post["order_id"];
            if(isset($data_post["payment_type"])) $data_insert["payment_type"]             = $data_post["payment_type"];
            if(isset($data_post["transaction_status"])) $data_insert["transaction_status"] = $data_post["transaction_status"];
            if(isset($data_post["gross_amount"])) $data_insert["gross_amount"]             = $data_post["gross_amount"];
            if(isset($data_post["fraud_status"])) $data_insert["fraud_status"]             = $data_post["fraud_status"];
            if(isset($data_post["approval_code"])) $data_insert["approval_code"]           = $data_post["approval_code"];
            if(isset($data_post["signature_key"])) $data_insert["signature_key"]           = $data_post["signature_key"];
            if(isset($data_post["transaction_time"])){
                $datetime = strtotime($data_post["transaction_time"]);
                $data_insert["transaction_time"] = date("Y-m-d H:i:s", $datetime);
            }
            if(isset($data_post["status_message"])) $data_insert["status_message"]         = $data_post["status_message"];
            if(isset($data_post["status_code"])) $data_insert["status_code"]               = $data_post["status_code"];
            if(isset($data_post["biller_code"])) $data_insert["biller_code"]               = $data_post["biller_code"];
            if(isset($data_post["bill_key"])) $data_insert["bill_key"]                     = $data_post["bill_key"];
            if(isset($data_post["custom_field3"])) $data_insert["custom_field3"]           = $data_post["custom_field3"];
            if(isset($data_post["custom_field2"])) $data_insert["custom_field2"]           = $data_post["custom_field2"];
            if(isset($data_post["custom_field1"])) $data_insert["custom_field1"]           = $data_post["custom_field1"];
            if(isset($data_post["masked_card"])) $data_insert["masked_card"]               = $data_post["masked_card"];
            if(!empty($data_post['bank'])){
                if(isset($data_post["bank"])) $data_insert["bank"]                             = $data_post["bank"];
            }
            // BNI && ATM Network
            if(!empty($data_post["va_numbers"])) {
                foreach($data_post['va_numbers'] as $van){
                    $data_insert["bank"] = $van['bank'];
                    $data_insert["va_numbers"] = $van['va_number'];
                }
            }
            // mandiri
            if ($data_post['payment_type'] == 'echannel') {
                $data_insert["bank"] = 'mandiri';
                $data_insert["va_numbers"] = $data_post['bill_key'];
            }
            // Permata
            if (!empty($data_post['permata_va_number'])) {
                $data_insert['bank'] = 'permata';
                $data_insert['va_numbers'] = $data_post['permata_va_number'];
            }

            //BCA KlikPay

            // Klik BCA

            // CIMB Cliks

            //Mandiri KlikPay

            //Indomaret

            if (!empty($data_post['store'])) {
                if ($data_post['store'] == 'indomaret') {
                    $data_insert['bank'] = 'indomaret';
                    $data_insert['va_numbers'] = $data_post['payment_code'];
                }
            }

            //Gopay

            //Danamon Online Bank

            if(isset($data_post["eci"])) $data_insert["eci"]                               = $data_post["eci"];
            if(isset($data_post["pdf_url"])) $data_insert["pdf_url"]                               = $data_post["pdf_url"];

            $data_insert["log_json"]                               = json_encode($data_post);
            $data_transaction = $this->db->get_where('midtrans', array("transaction_id" => $data_post["transaction_id"], "order_id
                " => $data_post["order_id"]));
            $user_idx = "";
            $book_idx = "";
            if ($data_transaction->num_rows() > 0) {
                $user_idx = $data_transaction->row()->user_id;
                $book_idx = $data_transaction->row()->book_id;
                $data_insert['user_id'] = $user_idx;
                $data_insert['book_id'] = $book_idx;
            }
            $param_id = array('transaction_id'=>$data_post['transaction_id'], 'order_id'=>$data_post['order_id']);;
            if ($this->db->get_where('midtrans', $param_id)->num_rows() > 0) {
                $ins = $this->db->update("midtrans", $data_insert, $param_id);
                if (!empty($user_idx) && !empty($book_idx)) {
                    if(isset($data_insert["transaction_status"]) && $data_insert["transaction_status"] == "settlement") {
                        $this->db->select("id_book");
                        $cekCollection = $this->db->get_where("collections", array("id_user" => $user_idx, "id_book" => $book_idx));
                        if($cekCollection->num_rows() == 0) $this->db->insert("collections", array("id_user" => $user_idx, "id_book" => $book_idx));
                    }
                    $books = $this->db->get_where("book", array("book_id"=>$book_idx));
                    $status = "sedang menunggu proses pembayaran";
                    if ($data_insert['transaction_status'] == "pending" || $data_insert['transaction_status'] == "capture") $status = "sedang menunggu proses pembayaran";
                    elseif ($data_insert['transaction_status'] == "cancel" || $data_insert['transaction_status'] == "deny") {
                        $status = "sudah di batalkan";
                    }elseif($data_insert["transaction_status"] == "settlement"){
                        $status = "telah lunas";
                    }
                    //check if books query > 0
                    if($books->num_rows() > 0) {
                        $notif_text = "Status Pembayaran Anda untuk " . $books->row()->title_book . " " . $status;
                        $arr_notif = array(
                            "notif_type" => REST_Controller::type_transaction,
                            "notif_to" => $user_idx,
                            "notif_by" => 0,
                            "notif_text" => $notif_text
                        );
                        $this->db->insert("notifications", $arr_notif);
                        $insd = $this->db->insert_id();
                        if ($insd) {
                            setpush_notif($user_idx, $notif_text, REST_Controller::type_transaction, $user_idx);
                        }
                    }
                }
            }else{
                $ins = $this->db->insert("midtrans", $data_insert);
            }
            //check if $ins success
            if($ins){
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "Insert success";
                $responses = array(
                    "code" => $resp_code,
                    "message" => $resp_message,
                    "data" => $this->db->get_where("midtrans",array("id"=>$this->db->insert_id()))->result(),
                );
            }
            else{
                $http_response_header = REST_Controller::HTTP_NOT_FOUND;
                $resp_code = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "Insert Error";
                $responses = array(
                    "code" => $resp_code,
                    "message" => $resp_message,
                    "data" => null,
                );
            }
            $this->set_response($responses, $http_response_header);
        }
    }

    public function index_put()
    {
        $body_post = (object)$this->put();
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $user_id = $this->post("user_id");
        $book_id = $this->post("book_id");
        $pdf_url = $this->post("pdf_url");
        $data_param = array(
            "transaction_id" => $body_post->transaction_id,
            "order_id"       => $body_post->order_id
        );
        $data_update = array(
            "user_id"       => $user_id,
            "book_id"       => $book_id,
            "pdf_url"       => $pdf_url
        );
        $update_transaction = $this->db->update("midtrans", $data_update, $data_param);
        if($update_transaction){
            $this->db->select("payment_type, transaction_status");
            $get_midtrans = $this->db->get_where("midtrans", $data_param);
            if($get_midtrans->num_rows() > 0 && ($get_midtrans->row()->payment_type == REST_Controller::payment_mandiri_clickpay || $get_midtrans->row()->payment_type == REST_Controller::payment_cimb_clicks || $get_midtrans->row()->payment_type == REST_Controller::payment_bca_klikpay  || $get_midtrans->row()->payment_type == REST_Controller::payment_gojek )){
                if (!empty($user_id) && !empty($book_id)) {
                    if($get_midtrans->row()->transaction_status == REST_Controller::stat_settlement) {
                        $this->db->select("id_book");
                        $cekCollection = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $book_id));
                        if($cekCollection->num_rows() == 0) {
                            $this->db->insert("collections", array("id_user" => $user_id, "id_book" => $book_id));
                        }
                    }
                    $books = $this->db->get_where("book", array("book_id"=>$book_id));
                    $status = "sedang menunggu proses pembayaran";
                    if ($get_midtrans->row()->transaction_status == REST_Controller::stat_pending || $get_midtrans->row()->transaction_status == REST_Controller::stat_capture) $status = "sedang menunggu proses pembayaran";
                    elseif ($get_midtrans->row()->transaction_status == REST_Controller::stat_cancel || $get_midtrans->row()->transaction_status == REST_Controller::stat_deny) {
                        $status = "sudah di batalkan";
                    }elseif($get_midtrans->row()->transaction_status == REST_Controller::stat_settlement){
                        $status = "telah lunas";
                    }
                    //check if books query > 0
                    if($books->num_rows() > 0) {
                        $notif_text = "Status Pembayaran Anda untuk " . $books->row()->title_book . " " . $status;
                        $arr_notif = array(
                            "notif_type" => REST_Controller::type_transaction,
                            "notif_to" => $user_id,
                            "notif_by" => 0,
                            "notif_text" => $notif_text
                        );
                        $this->db->insert("notifications", $arr_notif);
                        $insd = $this->db->insert_id();
                        if ($insd) {
                            setpush_notif($user_id, $notif_text, REST_Controller::type_transaction, $user_id);
                        }
                    }
                }
            }
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "Update success";
            $responses = array(
                "code" => $resp_code,
                "message" => $resp_message,
                "data" => (object)$this->db->get_where('midtrans',array('transaction_id' =>$body_post->transaction_id,
                    'order_id' => $body_post->order_id,
                    'transaction_status' => 'pending'))->row(),
            );
        }
        else{
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "Update Error , check your parameter";
            $responses = array(
                "code" => $resp_code,
                "message" => $resp_message,
                "data" => null,
            );
        }
        $this->set_response($responses, $http_response_header);
    }

    public function pending_post()
    {
        $datas = array();
        $body_post = (object)$this->post();
        $data_param = array(
            "user_id"            => $body_post->user_id,
            "transaction_status" => "pending"
        );
        if (!empty($body_post->book_id)) {
            $data_param['book_id'] = $body_post->book_id;
        }
        $sql_trans = $this->db->get_where('midtrans',$data_param);
        $data_trans = $sql_trans->row();
        if ($sql_trans->num_rows() > 0) {
            if ($sql_trans->num_rows() < 2) {
                $this->db->select("book.*,users.fullname as author, users.prof_pict as author_pict");
                $this->db->join("users", "users.user_id = book.author_id");
                $sql_book = $this->db->get_where('book',array("book_id" => $data_trans->book_id));
                if ($sql_book->num_rows() > 0) {
                    $data_book = $sql_book->row();

                    $is_bookmark = "false";
                    if (!empty($data["user_id"])) {
                        $this->db->where("id_user", $data["user_id"]);
                        $this->db->where("id_book", $data_book->book_id);
                        $sbookmark = $this->db->get("bookmark");
                        if ($sbookmark->num_rows() > 0) $is_bookmark = "true";
                        if (empty($this->post("chapter_id")) && $data_book->status_publish == "publish") {
                            $data_update = array();
                            $this->db->select("view_count");
                            $cgs = $this->db->get_where("book", array("book_id" => $data["book_id"]));
                            if ($cgs->num_rows() > 0) $view_count = (int)$cgs->row()->view_count + 1;
                            $data_update['view_count'] = $view_count;
                            $this->db->update("book", $data_update, array('book_id' => $data["book_id"]));
                            $gethist = array("latest_book" => $data["book_id"], "reader" => $data["user_id"]);
                            $chist = $this->db->get_where("history_read", $gethist);
                            if ($chist->num_rows() > 0) $this->db->update("history_read", array("latest_read" => date("Y-m-d H:i:s")), $gethist);
                            else $this->db->insert("history_read", $gethist);
                        }
                    }
                    $count_comment = 0;
                    $getcomment = $this->db->get_where("comment", array("id_book" => $data_book->book_id));
                    if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
                    $is_follow = "false";
                    $cekfollow = $this->db->get_where("follows", array("created_by" => $this->post("user_id"), "is_follow" => $data_book->author_id));
                    if ($cekfollow->num_rows() > 0) $is_follow = "true";
                    $is_likes = "false";
                    $getlikes = $this->db->get_where("likes", array("id_book" => $data_book->book_id));
                    if ($getlikes->num_rows() > 0) {
                        $is_likes = "true";
                    }
                    $is_free = "false";
                    if ($data_book->is_free == "free") {
                        $is_free = "true";
                    }
                    $price = 0;
                    if ($is_free == "false") {
                        $price = (double)$data_book->total_price;
                    }

                    $book_info['book_id'] = $data_book->book_id;
                    $book_info['title_book'] = $data_book->title_book;
                    $book_info['view_count'] = $data_book->view_count;
                    $book_info['like_count'] = $data_book->like_count;
                    $book_info['is_like'] = $is_likes;
                    $book_info['is_free'] = $is_free;
                    $book_info['book_price'] = $price;
                    $book_info['is_bookmark'] = $is_bookmark;
                    $book_info['share_count'] = $data_book->share_count;
                    $book_info['book_comment_count'] = $count_comment;
                    $book_info['cover_url'] = $data_book->file_cover;

                    $data['book_info'] = $book_info;

                    $author_info['author_id'] = $data_book->author_id;
                    $author_info['author_name'] = $data_book->author;
                    $author_info['avatar'] = $data_book->author_pict;
                    $author_info['is_follow'] = $is_follow;

                    $data['author'] = $author_info;

                    $http_response_header = REST_Controller::HTTP_OK;
                    $resp_code = REST_Controller::HTTP_ACCEPTED;
                    $resp_message = "Show book Success";
                    $responses = array(
                        "code" => $resp_code,
                        "message" => $resp_message,
                        "data" => $data,
                    );
                }else{
                    $http_response_header = REST_Controller::HTTP_ACCEPTED;
                    $resp_code = REST_Controller::HTTP_NOT_FOUND;
                    $resp_message = "book not found";
                    $responses = array(
                        "code" => $resp_code,
                        "message" => $resp_message,
                        "data" => array(),
                    );
                }
            }else{
                foreach ($sql_trans->result() as $exp_book) {
                    $book_id[] = $exp_book->book_id;
                }
                $book = implode(',', $book_id);

                $sql_book = $this->db->query("select book.*,users.fullname as author, users.prof_pict as author_pict from book join users on users.user_id = book.author_id where book_id in($book)");
                // print_r($sql_book->result());
                foreach ($sql_book->result() as $data_book) {
                    $is_bookmark = "false";
                    if (!empty($data["user_id"])) {
                        $this->db->where("id_user", $data["user_id"]);
                        $this->db->where("id_book", $data_book->book_id);
                        $sbookmark = $this->db->get("bookmark");
                        if ($sbookmark->num_rows() > 0) $is_bookmark = "true";
                        if (empty($this->post("chapter_id")) && $data_book->status_publish == "publish") {
                            $data_update = array();
                            $this->db->select("view_count");
                            $cgs = $this->db->get_where("book", array("book_id" => $data["book_id"]));
                            if ($cgs->num_rows() > 0) $view_count = (int)$cgs->row()->view_count + 1;
                            $data_update['view_count'] = $view_count;
                            $this->db->update("book", $data_update, array('book_id' => $data["book_id"]));
                            $gethist = array("latest_book" => $data["book_id"], "reader" => $data["user_id"]);
                            $chist = $this->db->get_where("history_read", $gethist);
                            if ($chist->num_rows() > 0) $this->db->update("history_read", array("latest_read" => date("Y-m-d H:i:s")), $gethist);
                            else $this->db->insert("history_read", $gethist);
                        }
                    }
                    $count_comment = 0;
                    $getcomment = $this->db->get_where("comment", array("id_book" => $data_book->book_id));
                    if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
                    $is_follow = "false";
                    $cekfollow = $this->db->get_where("follows", array("created_by" => $this->post("user_id"), "is_follow" => $data_book->author_id));
                    if ($cekfollow->num_rows() > 0) $is_follow = "true";
                    $is_likes = "false";
                    $getlikes = $this->db->get_where("likes", array("id_book" => $data_book->book_id));
                    if ($getlikes->num_rows() > 0) {
                        $is_likes = "true";
                    }
                    $is_free = "false";
                    if ($data_book->is_free == "free") {
                        $is_free = "true";
                    }
                    $price = 0;
                    if ($is_free == "false") {
                        $price = (double)$data_book->total_price;
                    }

                    $book_info['book_id'] = $data_book->book_id;
                    $book_info['title_book'] = $data_book->title_book;
                    $book_info['view_count'] = $data_book->view_count;
                    $book_info['like_count'] = $data_book->like_count;
                    $book_info['is_like'] = $is_likes;
                    $book_info['is_free'] = $is_free;
                    $book_info['book_price'] = $price;
                    $book_info['is_bookmark'] = $is_bookmark;
                    $book_info['share_count'] = $data_book->share_count;
                    $book_info['book_comment_count'] = $count_comment;
                    $book_info['cover_url'] = $data_book->file_cover;

                    $data['book_info'] = $book_info;

                    $author_info['author_id'] = $data_book->author_id;
                    $author_info['author_name'] = $data_book->author;
                    $author_info['avatar'] = $data_book->author_pict;
                    $author_info['is_follow'] = $is_follow;

                    $data['author'] = $author_info;

                    $datas[] = $data;
                }
                $http_response_header = REST_Controller::HTTP_OK;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "Your list pending transaction";
                $responses = array(
                    "code" => $resp_code,
                    "message" => $resp_message,
                    "data" => $datas
                );
            }
        }else{
            $http_response_header = REST_Controller::HTTP_ACCEPTED;
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "Data transaction not found";
            $responses = array(
                "code" => $resp_code,
                "message" => $resp_message,
                "data" => array(),
            );
        }
        $this->set_response($responses, $http_response_header);
    }
    public function reminder_get()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;

        $datas = array();
        $body_post = (object)$this->post();
        $data_param = array(
            "user_id"            => $user_id
        );
        $in_transaction_status = array(REST_Controller::stat_pending, REST_Controller::stat_capture);
        $this->db->where_in('transaction_status', $in_transaction_status);
        $this->db->order_by('transaction_date', 'DESC');
        $sql_trans = $this->db->get_where('midtrans',$data_param);
        $data_trans = $sql_trans->row();
        if ($sql_trans->num_rows() > 0) {
            foreach ($sql_trans->result() as $data_trans) {
                $data_book = $this->db->query("select book.*,users.fullname as author, users.prof_pict as author_pict from book join users on users.user_id = book.author_id where book_id = '".$data_trans->book_id."'")->row();
                $is_bookmark = "false";
                if (!empty($data["user_id"])) {
                    $this->db->where("id_user", $data["user_id"]);
                    $this->db->where("id_book", $data_book->book_id);
                    $sbookmark = $this->db->get("bookmark");
                    if ($sbookmark->num_rows() > 0) $is_bookmark = "true";
                    if (empty($this->post("chapter_id")) && $data_book->status_publish == "publish") {
                        $data_update = array();
                        $this->db->select("view_count");
                        $cgs = $this->db->get_where("book", array("book_id" => $data["book_id"]));
                        if ($cgs->num_rows() > 0) $view_count = (int)$cgs->row()->view_count + 1;
                        $data_update['view_count'] = $view_count;
                        $this->db->update("book", $data_update, array('book_id' => $data["book_id"]));
                        $gethist = array("latest_book" => $data["book_id"], "reader" => $data["user_id"]);
                        $chist = $this->db->get_where("history_read", $gethist);
                        if ($chist->num_rows() > 0) $this->db->update("history_read", array("latest_read" => date("Y-m-d H:i:s")), $gethist);
                        else $this->db->insert("history_read", $gethist);
                    }
                }
                $is_free = "false";
                if ($data_book->is_free == "free") {
                    $is_free = "true";
                }
                $price = 0;
                if ($is_free == "false") {
                    $price = (double)$data_book->total_price;
                }

                $book_info['book_id'] = $data_book->book_id;
                $book_info['order_id'] = $data_trans->order_id;
                $book_info['transaction_id'] = $data_trans->transaction_id;
                $book_info['title_book'] = $data_book->title_book;
                $book_info['book_price'] = $price;
                $book_info['cover_url'] = $data_book->file_cover;

                $book_info['payment_type'] = $data_trans->payment_type;
                $desc_payment = "";
                if ($book_info['payment_type'] == REST_Controller::payment_bank_transfer) {
                    if ($book_info['bank'] == REST_Controller::bank_bni) {
                        $desc_payment = "Bank Transfer BNI";
                    }elseif ($book_info['bank'] == REST_Controller::bank_bca) {
                        $desc_payment = "Bank Transfer BCA";
                    }elseif ($book_info['bank'] == REST_Controller::bank_permata) {
                        $desc_payment = "Bank Transfer Permata";
                    }elseif ($book_info['bank'] == REST_Controller::bank_mandiri) {
                        $desc_payment = "Bank Transfer Mandiri";
                    }else{
                        $desc_payment = "Bank Transfer";
                    }
                }
                if ($book_info['payment_type'] == REST_Controller::payment_cstore) {
                    $desc_payment = "Indomaret";
                }
                if ($book_info['payment_type'] == REST_Controller::payment_danamon_online) {
                    $desc_payment = "Danamon Online Banking";
                }
                if ($book_info['payment_type'] == REST_Controller::payment_gojek) {
                    $desc_payment = "Go-Pay";
                }
                if ($book_info['payment_type'] == REST_Controller::payment_bca_klikbca) {
                    $desc_payment = "Klik BCA";
                }
                if ($book_info['payment_type'] == REST_Controller::payment_bca_klikpay) {
                    $desc_payment = "BCA Klik Pay";
                }
                if ($book_info['payment_type'] == REST_Controller::payment_credit_card) {
                    $desc_payment = "Credit Card";
                }
                if ($book_info['payment_type'] == REST_Controller::payment_cimb_clicks) {
                    $desc_payment = "CIMB Clicks";
                }
                if ($book_info['payment_type'] == REST_Controller::payment_echannel) {
                    $desc_payment = "Bank Transfer Mandiri";
                }
                $book_info['payment_type_desc'] = $desc_payment;
                $desc_stat = "";
                $book_info['transaction_status'] = $data_trans->transaction_status;
                if ($book_info['transaction_status'] == REST_Controller::stat_pending || $book_info['transaction_status'] == REST_Controller::stat_capture) {
                    $desc_stat = "Menunggu proses pembayaran";
                }
                if ($book_info['transaction_status'] == REST_Controller::stat_cancel || $book_info['transaction_status'] == REST_Controller::stat_deny) {
                    $desc_stat = "Pembayaran anda di batalkan";
                }
                $book_info['transaction_desc'] = $desc_stat;
                $book_info['gross_amount'] = $data_trans->gross_amount;
                $book_info['bank'] = $data_trans->bank;
                $book_info['va_numbers'] = $data_trans->va_numbers;
                $book_info['pdf_url'] = $data_trans->pdf_url;
                $datas[] = $book_info;
            }
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;
            $resp_message = "Your list pending transaction";
            $responses = array(
                "code" => $resp_code,
                "message" => $resp_message,
                "data" => $datas
            );
        }else{
            $http_response_header = REST_Controller::HTTP_ACCEPTED;
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "Data transaction not found";
            $responses = array(
                "code" => $resp_code,
                "message" => $resp_message,
                "data" => array(),
            );
        }

        set_resp($responses, $http_response_header);
    }

    public function cancel_post(){
        $order_trans = $this->post("order_id");
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = $http_response_header;
        $resp_message = "transaction cannot be empty";
        $resp_data = (object)array();
        if(!empty($order_trans)){
            $getmd = $this->db->get_where("midtrans", "order_id = $order_trans OR transaction_id = $order_trans");
            if($getmd->num_rows() > 0){
                $api_url = MIDTRANS_URL."/v2/$order_trans/cancel";
                $result = $this->midtrans_api($api_url, "POST");
                if($result){
                    $jsresult = json_decode($result);
                    if (isset($jsresult->status_code) && $jsresult->status_code == 200) {
                        $resp_code = REST_Controller::HTTP_OK;
                        $resp_message = "success, transaction is canceled";
                    }else{
                        $resp_code = REST_Controller::HTTP_MULTIPLE_CHOICES;
                        $resp_message = "sorry, something happens, will inform you soon";
                        // $resp_data = $jsresult;
                    }
                }else{
                    $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                    $resp_message = "something wrong with midtrans, will info you soon!";
                }
            }else{
                $resp_message = REST_Controller::HTTP_NOT_FOUND;
                $resp_message = "transaction not found";
            }
        }
        $resps = array(
            "code"  => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($resps);
    }

    private function midtrans_api($api_url, $request_type = "GET", $request_body = null)
    {
        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode(MIDTRANS_KEY . ':')
            )
        );
        if ($request_type == "GET") {
            $curl_options[CURLOPT_CUSTOMREQUEST] = "GET";
            $curl_options[CURLOPT_POST] = false;
            $curl_options[CURLOPT_SSL_VERIFYPEER] = 0;
        } else {
            if ($request_type == "PATCH") $curl_options[CURLOPT_CUSTOMREQUEST] = "PATCH";
            $curl_options[CURLOPT_POST] = true;
            if (!empty($request_body)) $curl_options[CURLOPT_POSTFIELDS] = json_encode($request_body);
        }
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        if ($result) {
            return $result;
        } else {
            return curl_error($ch);
        }
    }
}
