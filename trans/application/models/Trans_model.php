<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Trans_model extends CI_Model
{
    // get all orders or get one by id
    public function getTrans($id = NULL)
    {
        if ($id === NULL) {
            return $this->db->get('transactions')->result_array();
        }

        return $this->db->get_where('transactions', ['id' => $id])->row_array();
    }

    // create a new order
    public function createTrans($data)
    {
        $this->db->insert('transactions', $data);
        return $this->db->affected_rows();
    }

    // update an existing order
    public function updateTrans($data, $id)
    {
        $this->db->update('transactions', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

    // delete an existing order
    public function deleteTrans($id)
    {
        $this->db->delete('transactions', ['id' => $id]);
        return $this->db->affected_rows();
    }

}