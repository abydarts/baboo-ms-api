<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Books extends REST_Controller
{
    // constructor for the class
    public function __construct(){
        parent::__construct();
        $this->load->model('Books_model', 'books');
        $this->load->helper("baboo");
        $this->load->helper("books");
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $a = base_url();
        $this->bucket = REST_Controller::bucket_staging;
        if (strpos($a, REST_Controller::api_prod) !== false) {
            $this->bucket = REST_Controller::bucket_prod;
        }
        $this->methods['books_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['books_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['books_put']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['books_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->ParentUploadir = "../../uploads/";
        $this->Dirdir = "/uploads/"; #for server use
        $this->Uploadir = $this->ParentUploadir . "baboo-cover/";
        $this->PDFUploadir = $this->ParentUploadir . "baboo-docs/";
        $this->Videodir = $this->ParentUploadir . "baboo-cover/";
        $this->DUploadir = $this->Dirdir . "baboo-cover/"; #for server use
        $this->thumbdir = $this->ParentUploadir . "baboo-thumbs/";
        $this->load->library("CloudStorage", NULL, "s3");
        
    }

    // default GET method
    public function index_get()
    {
        // Books from a data store e.g. database
        $this->db->select("book.book_id as id, chapters.chapter_id as id_chapter, chapters.chapter_title as judul_chapter, users.user_id as author_id, users.fullname as authorname");
        $this->db->join("users", "users.user_id = book.user_id");
        $this->db->join("chapters", "chapters.id_book = book.book_id");
        $books = $this->db->get()->result();
        $id = $this->get('book_id');
        // If the id parameter doesn't exist return all the books
        if ($id === NULL) {   // Check if the books data store contains books (in case the database result returns NULL)
            if ($books) {
                $resp_books = array(
                    "code" => REST_Controller::HTTP_OK,
                    "message" => "",
                    "data" => $books
                );
                // Set the response and exit
                $this->response($resp_books, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $this->response([
                    'code' => REST_Controller::HTTP_NOT_FOUND,
                    'message' => 'No books were found',
                    'data' => (object)array()
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
        // Find and return a single record for a particular books.
        $id = (int)$id;
        // Validate the id.
        if ($id <= 0) {
            // Invalid id, set the response and exit.
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => 'What are you looking for?',
                'data' => (object)array()
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (400) being the HTTP response code
        }
        // Get the book from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.
        $book = NULL;
        //check if books array is not empty
        if (!empty($books)) {
            foreach ($books as $key => $value) {
                if ($value->book_id && $value->book_id == $id) {
                    $book = $value;
                }
            }
        }
        //check if your selected book array not empty
        if (!empty($book)) {
            $resp_book = array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "",
                "data" => $book
            );
            set_resp($resp_book, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $this->response([
                'code' => REST_Controller::HTTP_NOT_FOUND,
                'message' => 'No books were found',
                'data' => (object)array()
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    // default POST method
    public function index_post()
    {
        $book_id = $this->post("book_id");
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "get all chapter success";
        $data_chapter = array();
        $book_info = array();
        $chapters = array();
        $status_publish = 1;
        $this->db->where(array("book_id" => $book_id));
        $books = $this->db->get("book");
        $is_free = false;
        $is_bought = false;
        $is_download = false;
        $valid = true;
        $stat_publish = "publish";
        if ($books->num_rows() > 0) {
            $stat_publish = $books->row()->status_publish;
            $this->db->select("is_download");
            $cekBought = $this->db->get_where("collections", array("id_user" => $user_id, "id_book" => $book_id));
            if ($cekBought->num_rows() > 0) {
                $is_bought = true;
                if (isset($cekBought->row()->is_download) && $cekBought->row()->is_download == 1) $is_download = true;
            }
            if ($stat_publish == "on editor") $stat_publish = "edit publish";
            if ($books->row()->status_publish != "publish" && $books->row()->author_id == $user_id) {
                $status_publish = 2;
            }
            if ($books->row()->status_publish == "draft" && $books->row()->author_id != $user_id) {
                $valid = false;
            }
            //check if valid true
            if ($valid) {
                $count_comment = 0;
                $this->db->select("id_book");
                $getcomment = $this->db->get_where("comment", array("id_book" => $books->row()->book_id));
                if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
                $is_likes = false;
                $this->db->select("id_book");
                $getlikes = $this->db->get_where("likes", array("id_book" => $books->row()->book_id, "id_user" => $user_id));
                if ($getlikes->num_rows() > 0) {
                    $is_likes = true;
                }
                $is_bookmark = false;
                $this->db->where("id_user", $user_id);
                $this->db->where("id_book", $book_id);
                $this->db->select("id_book");
                $sbookmark = $this->db->get("bookmark");
                //validate and fill is_bookmark true when theres data on bookmark table
                if ($sbookmark->num_rows() > 0) $is_bookmark = true;
                if ($books->row()->is_free == "free" || $is_bought == true) {
                    $is_free = true;
                }

                $price = 0;
                if ($is_free == false) {
                    $price = (double)$books->row()->total_price;
                }
                $titleBook = $books->row()->title_book;
                if (!empty($books->row()->title_draft)) {
                    $titleBook = $books->row()->title_draft;
                }
                $book_info = array(
                    "book_id" => $books->row()->book_id,
                    "title_book" => (string)$titleBook,
                    "view_count" => (int)$books->row()->view_count,
                    "like_count" => (int)$books->row()->like_count,
                    "is_like" => $is_likes,
                    "is_bought" => $is_bought,
                    "is_download" => $is_download,
                    "is_free" => $is_free,
                    "book_price" => $price,
                    "is_bookmark" => $is_bookmark,
                    "share_count" => (int)$books->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "cover_url" => (string)$books->row()->file_cover
                );
            } else {
                $resp_code = REST_Controller::HTTP_FORBIDDEN;
                $resp_message = "unauthorized";
            }
        }
        $data_chapter["book_info"] = (object)$book_info;
        if ($status_publish == 1) $this->db->where("status_publish", $status_publish);
        $this->db->where("id_book", $book_id);
        $dcg = $this->db->get("chapter");
        if (empty($book_id)) {
            $resp_code = REST_Controller::HTTP_BAD_REQUEST;
            $resp_message = "book id cannot empty";
        }

        //check
        if ($dcg->num_rows() > 0 && $valid !== false) {
            $limit_text = REST_Controller::limitDescChapter;
            foreach ($dcg->result() as $dckey => $dcval) {
                $stat_draft = $dcval->status_publish;
                $stat_free = $dcval->status_free;
                $statey = "publish";
                if ($stat_draft > 1) {
                    if ($stat_draft == 2 && $stat_publish == "edit publish") $statey = "edit publish";
                    else $statey = "draft";
                }
                $res_par_text = "";
                $chapter_title = $dcval->chapter_title;
                if ($status_publish == 1) {
                    $gepar = $this->db->get_where("paragraph", array("id_chapter" => $dcval->chapter_id));
                } else {
                    if (!empty($dcval->chapter_draft)) $chapter_title = $dcval->chapter_draft;
                    $gepar = $this->db->get_where("draft_paragraph", array("id_chapter" => $dcval->chapter_id));
                    if ($gepar->num_rows() == 0) $gepar = $this->db->get_where("paragraph", array("id_chapter" => $dcval->chapter_id));
                }
                foreach ($gepar->result() as $par_result => $paragraph) {
                    $res_par_text .= strip_tags_with_whitespace($paragraph->txt_paragraph);
                    $limit_count = $gepar->num_rows() - 1;
                    if ($par_result != $limit_count) $res_par_text .= " ";
                    if (strlen($res_par_text) > $limit_text) {
                        $res_par_text = substr($res_par_text, 0, $limit_text) . "...";
                        break;
                    }
                }
                $data_free = false;
                if ($stat_free == "free" || $is_bought == true) {
                    $data_free = true;
                }
                $chapters[] = array(
                    "chapter_id" => $dcval->chapter_id,
                    "chapter_title" => $chapter_title,
                    "desc" => trim($res_par_text),
                    "status_publish" => setStats($statey),
                    "chapter_free" => $data_free
                );
            }
            // $data_chapter = $dcg->result_array();
            $http_response_header = REST_Controller::HTTP_OK;
            $resp_code = REST_Controller::HTTP_OK;

        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "chapter not found or unauthorized";
        }
        $data_chapter["chapter"] = $chapters;
        $resp_array = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$data_chapter,
        );
        set_resp($resp_array, $http_response_header);
    }

    // default PUT method
    public function index_put()
    {
        $book_id = $this->put("book_id");
        $data = $this->put();
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $this->db->select("book_id as id, category_id, price, total_price, is_free, view_count, like_count, share_count, title_book, title_draft, author_id, IFNULL(file_cover,'') as cover_url, fullname as author, prof_pict as author_pict, status_publish, publish_at, latest_update");
        $this->db->from("book");
        $this->db->join("users", "users.user_id = book.author_id");
        $this->db->where(array('book_id' => $data['book_id']));
        $prevQuery = $this->db->get();
        $prevCheck = $prevQuery->num_rows();
        $books = array();
        $http_response = REST_Controller::HTTP_ACCEPTED;
        if ($prevCheck > 0) {
            $is_bookmark = false;
            $is_likes = false;
            $publish_date = $prevQuery->row()->latest_update;
            if ($prevQuery->row()->status_publish != "draft") {
                $publish_date = $prevQuery->row()->publish_at;
            }
            $count_comment = 0;
            $getcomment = $this->db->get_where("comment", array("id_book" => $prevQuery->row()->id));
            if ($getcomment->num_rows() > 0) $count_comment = $getcomment->num_rows();
            $is_follow = false;
            $cekfollow = $this->db->get_where("follows", array("created_by" => $user_id, "is_follow" => $prevQuery->row()->author_id));
            if ($cekfollow->num_rows() > 0) $is_follow = true;
            $is_free = false;
            if ($prevQuery->row()->is_free == "free") {
                $is_free = true;
            }
            $price = 0;
            if ($is_free == false) {
                $price = (double)$prevQuery->row()->total_price;
            }
            $titleBook = (string)$prevQuery->row()->title_book;
            if (!empty($prevQuery->row()->title_draft)) $titleBook = $prevQuery->row()->title_draft;
            $prevResult = array(
                "book_info" => array(
                    "book_id" => $prevQuery->row()->id,
                    "title_book" => $titleBook,
                    "view_count" => (int)$prevQuery->row()->view_count,
                    "like_count" => (int)$prevQuery->row()->like_count,
                    "is_bookmark" => $is_bookmark,
                    "is_like" => $is_likes,
                    "is_free" => $is_free,
                    "book_price" => $price,
                    "share_count" => (int)$prevQuery->row()->share_count,
                    "book_comment_count" => (int)$count_comment,
                    "latest_update" => time_ago($prevQuery->row()->latest_update),
                    "publish_date" => time_ago($publish_date),
                    "cover_url" => $prevQuery->row()->cover_url
                ),
                "author" => array(
                    "author_id" => $prevQuery->row()->author_id,
                    "author_name" => $prevQuery->row()->author,
                    "avatar" => (string)$prevQuery->row()->author_pict,
                    "isFollow" => $is_follow
                )
            );

            $data_category = array();
            if (isset($data['chapter']) && !empty($data['chapter'])) {
                $this->db->where(array('chapter_id' => $data['chapter']));
            } else {
                $this->db->order_by("chapter_id", "asc");
            }
            $this->db->where(array("id_book" => $data["book_id"]));
            $gchap = $this->db->get("chapter");
            $paragraph = array();
            if ($gchap->num_rows() > 0) {
                $chav = $gchap->row();
                //checking part of urutan book
                $keyes = getIndexChapter($chav->id_book, $chav->chapter_id);
                $chapter_free = true;
                $limitfree = REST_Controller::limitFree;
                if ($is_free == false && $keyes >= $limitfree) {
                    $chapter_free = false;
                }
                $chaptitle = (string)$chav->chapter_title;
                if (!empty($chav->chapter_draft)) $chaptitle = $chav->chapter_draft;
                $paragraph = array(
                    "chapter_id" => $chav->chapter_id,
                    "chapter_title" => $chaptitle,
                    "chapter_free" => $chapter_free
                );
                $cgasal = 0;
                $gpar = $this->db->get_where("draft_paragraph", array("id_chapter" => $chav->chapter_id));
                if ($gpar->num_rows() > 0) {
                    $cgasal = $gpar->num_rows();
                    $gasal = $gpar->result();
                } else {
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $chav->chapter_id));
                    if ($gpar->num_rows() > 0) {
                        $cgasal = $gpar->num_rows();
                        $gasal = $gpar->result();
                    }
                }
                // check if result > 0
                if ($cgasal > 0) {
                    foreach ($gasal as $gpval) {
                        if (!$gpval->paragraph_id) {
                            $gpval->paragraph_id = $gpval->id_draftpar;
                        }
                        $par_text = $gpval->txt_paragraph;
                        if (substr($gpval->txt_paragraph, 0, 2) != "<p") {
                            $par_text = "<p>" . $gpval->txt_paragraph . "</p>";
                        } elseif (substr($gpval->txt_paragraph, -4) != "</p>") {
                            $par_text = $gpval->txt_paragraph . "</p>";
                        }
                        $count_commentp = 0;
                        $getcommentp = $this->db->get_where("comment", array("id_paragraph" => $gpval->paragraph_id));
                        if ($getcommentp->num_rows() > 0) $count_commentp = $getcommentp->num_rows();
                        $paragraph["paragraphs"][] = array(
                            "paragraph_id" => $gpval->paragraph_id,
                            "paragraph_text" => $par_text,
                            "comment_count" => (int)$count_commentp
                        );
                    }
                }
            }
            $this->db->select("id_catbook as category_id, cat_book as category_name");
            if (!empty($prevQuery->row()->category_id)) $this->db->where("id_catbook", $prevQuery->row()->category_id);
            else $this->db->order_by("id_catbook", "asc");
            $dct = $this->db->get("book_category");
            if ($dct->num_rows() > 0) $data_category = $dct->row();
            $prevResult["category"] = (object)$data_category;
            $prevResult["chapter"] = $paragraph;
            //set response
            $books["code"] = REST_Controller::HTTP_OK;
            $books["message"] = "show detail book success";
            $books["data"] = $prevResult;
            $http_response = REST_Controller::HTTP_OK;
        } else {
            $bbid = (object)array();
            $books["code"] = REST_Controller::HTTP_NOT_FOUND;
            $books["message"] = "Book Not Found";
            $books["data"] = $bbid;
        }
        set_resp($books, $http_response);
    }

    // default DELETE method
    public function index_delete()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $book_id = $this->uri->segment(4);
        $http_response_header = REST_Controller::HTTP_ACCEPTED;
        $resp_code = REST_Controller::HTTP_ACCEPTED;
        $resp_message = "draft book cannot be deleted";
        $resp_data = (object)array();
        $gbook = $this->db->get_where("book", array("book_id" => $book_id, "status_publish" => "draft", "author_id" => $user_id));
        if ($gbook->num_rows() > 0) {
            $gchap = $this->db->get_where("chapter", array("id_book" => $book_id));
            if ($gchap->num_rows() > 0) {
                foreach ($gchap->result() as $gchval) {
                    $gpar = $this->db->get_where("paragraph", array("id_chapter" => $gchval->chapter_id));
                    if ($gpar->num_rows() > 0) {
                        $this->db->delete("paragraph", array("id_chapter" => $gchval->chapter_id));
                    }
                }
                $this->db->delete("chapter", array("id_book" => $book_id));
            }
            $gdbb = $this->db->delete("book", array("book_id" => $book_id));
            if ($gdbb) {
                $resp_message = "Book has been deleted";
                $resp_code = REST_Controller::HTTP_OK;
                $http_response_header = REST_Controller::HTTP_OK;
            }
        }
        $resps = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => $resp_data
        );
        set_resp($resps, $http_response_header);
    }

    public function publish_post()
    {
        $key = sethead();
        $user_id = (!empty($key)) ? getUserFromKey($key) : 0;
        $resp_data = array();
        $book_id = $this->post("book_id");
        if (!empty($book_id)) {
            $this->db->where(array('book_id' => $book_id, "author_id" => $user_id));
            $gbook = $this->db->get("book");
            if ($gbook->num_rows() > 0) {
                $is_sellable = false;
                $is_publishable = false;
                $price = (string)$gbook->row()->price;
                $is_pdf = (string)$gbook->row()->is_pdf;
                $book_type = (string)$gbook->row()->book_type;
                $resp_code = REST_Controller::HTTP_OK;
                $resp_message = "get payment fee success";
                $res_par_text = "";
                $total_chap = 0;
                if ($is_pdf == 0) {
                    $this->load->helper("pdff");
                    $this->load->library("Fpdf_gen", "fpdf_gen");
                    $total_chap_sellable = REST_Controller::limitPDFJual;
                    $total_char_sellable = 0;
                    $pdf_url = (string)$gbook->row()->pdf_url;
                    $uploaddir = "../uploads/baboo-docs/";
                    $pdf = $uploaddir . "docs_" . time() . ".pdf";
                    file_put_contents($pdf, fopen($pdf_url, 'r'));
                    chmod($pdf, 0777);
                    if (file_exists($pdf)) {
                        $pdf_real = realpath($pdf);
                        $new_name = str_replace('.pdf', '', basename($pdf)) . "_decrypted.pdf";
                        $location_pdf = realpath($uploaddir) . "/$new_name";
                        $oldgenerateDate = (!empty($gbook->row()->generated_date)) ? date("Y-m-d H:i:s O", strtotime($gbook->row()->generated_date)) : date("Y-m-d H:i:s O");
                        $olddatapassword = 'ID#' . $book_id . '#' . $gbook->row()->title_book . '#' . strtotime($oldgenerateDate);
                        $oldpassword = hash_hmac('sha512', $olddatapassword, strtotime($oldgenerateDate));
                        exec("qpdf --decrypt $pdf_real --password=$oldpassword $location_pdf", $output, $return);
                        if (!$return) {
//                        $decrypted_pdf = $uploaddir.$new_name;
                            $newest_name = str_replace('.pdf', '', basename($pdf)) . "_1.4.pdf";
                            $locations_pdf = realpath($uploaddir) . "/$newest_name";
                            if (file_exists($uploaddir . $new_name)) chmod($uploaddir . $new_name, 0777);
                            $gs = "gs";
                            if (strpos(base_url(), REST_Controller::api_local) !== false) $gs = "gswin64c";
                            exec("$gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH -sOutputFile=$locations_pdf $location_pdf", $output, $return);
                            $total_chap = $this->fpdf_gen->countPage($uploaddir . $newest_name);
                            if (file_exists($pdf_real)) unlink($pdf_real);
                            if (file_exists($location_pdf)) unlink($location_pdf);
                            if (file_exists($locations_pdf)) unlink($locations_pdf);
                        }
                    }
                    if ($total_chap >= REST_Controller::limitPDFPublish) {
                        $is_publishable = true;
                        $resp_message = "your book is publish-able";
                        $total_chap_sellable = round((int)$total_chap * (REST_Controller::limitPDFJual / 100));
                        if ($total_chap >= $total_chap_sellable && $total_chap >= REST_Controller::limitPDFJualLimit) {
                            $is_sellable = true;
                        } else {
                            if ($total_chap < REST_Controller::limitPDFJual) {
                                $kurang_chap = REST_Controller::limitPDFJual - $total_chap;
                                $resp_message .= ", just need $kurang_chap more page(s) to sell your book";
                            }
                        }
                    } else {
                        $resp_message = "your book need more page to publish";
                    }

                } else {
                    $total_char_sellable = REST_Controller::limitSellableChar;
                    $total_chap_sellable = REST_Controller::limitSellable;
                    if ($book_type == 1) {
                        $schap = $this->db->get_where("chapter", array("id_book" => $book_id));
                        if ($schap && $schap->num_rows() > 0) {
                            $total_chap = $schap->num_rows();
                            $sval = $schap->result();
                            foreach ($sval as $svol) {
                                $gepar = $this->db->get_where("draft_paragraph", array("id_chapter" => $svol->chapter_id));
                                if ($gepar->num_rows() == 0) $gepar = $this->db->get_where("paragraph", array("id_chapter" => $svol->chapter_id));
                                foreach ($gepar->result() as $par_result => $paragraph) {
                                    $res_par_text .= strip_tags_with_whitespace($paragraph->txt_paragraph);
                                }
                            }
                        } else {
                            if (!empty($this->post("paragraph"))) {
                                $res_par_text .= strip_tags_with_whitespace($this->post("paragraph"));
                            }
                        }

                        if (strlen($res_par_text) >= REST_Controller::limitPublish) {
                            $is_publishable = true;
                            $resp_message = "your book is publish-able";
                            if ($total_chap >= REST_Controller::limitSellable && strlen($res_par_text) >= REST_Controller::limitSellableChar) {
                                $is_sellable = true;
                            } else {
                                $less_chap = false;
                                if ($total_chap < REST_Controller::limitSellable) {
                                    $less_chap = true;
                                    $kurang_chap = REST_Controller::limitSellable - $total_chap;
                                    $resp_message .= ", just need $kurang_chap more chapter(s)";
                                }
                                if (strlen($res_par_text) < REST_Controller::limitSellableChar) {
                                    $kurang_laman = round(REST_Controller::limitSellableChar / REST_Controller::limitsellperPage) - ceil(strlen($res_par_text) / REST_Controller::limitsellperPage);
                                    if ($less_chap == true) {
                                        $resp_message .= " and ";
                                    } else {
                                        $resp_message .= ", just need ";
                                    }
                                    $resp_message .= $kurang_laman . " page(s) more";
                                }
                                $resp_message .= " to sell your book";
                            }
                        } else {
                            $resp_message = "your book need more characters to publish";
                        }
                    } else {
                        $is_publishable = true;
                        $total_chap = 1;
                        $total_chap_sellable = 1;
                        $resp_message = "your book is publish-able";
                        $is_sellable = true;
                    }
                    //end if book_type
                }
                $resp_data["is_sellable"] = $is_sellable;
                $resp_data["total_chapter"] = $total_chap;
                $resp_data["total_chapter_sellable"] = $total_chap_sellable;
                $resp_data["char_total"] = strlen($res_par_text);
                $resp_data["char_total_sellable"] = $total_char_sellable;
                $resp_data["is_publishable"] = $is_publishable;
                $resp_data["payment_fee"] = REST_Controller::payment_fee;
                $resp_data["ppn"] = REST_Controller::tax_fee;
                $resp_data["writer_fee"] = REST_Controller::merchant_profit * 100;
                $resp_data["baboo_fee"] = REST_Controller::baboo_profit * 100;
                $resp_data["price"] = $price;
            } else {
                $resp_code = REST_Controller::HTTP_INTERNAL_SERVER_ERROR;
                $error = $this->db->error();
                $resp_message = "book not found because " . $error["code"] . ":" . $error["message"];
            }
        } else {
            $resp_code = REST_Controller::HTTP_NOT_FOUND;
            $resp_message = "book not found";
        }

        $response = array(
            "code" => $resp_code,
            "message" => $resp_message,
            "data" => (object)$resp_data
        );
        set_resp($response, $resp_code);
    }

}