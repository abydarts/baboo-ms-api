<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 5/23/2018
 * Time: 6:34 PM
 */
if (!class_exists("REST_Controller")) {
    require_once APPPATH . 'libraries/REST_Controller.php';
}

if (!function_exists("pdf_upload")) {

    function pdf_tmp_upload($book_id, $is_paid = false, $pdf_start = 0, $uploadDir = '../uploads/baboo-docs/')
    {
        $ci =& get_instance();
        $config['upload_path'] = $uploadDir;
        $config['allowed_types'] = 'pdf';
        //$config['max_size'] = 25000;
        $ci->load->library('upload', $config);
        if (!$ci->upload->do_upload('pdf_book')) {
            $resps = array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => strip_tags($ci->upload->display_errors()));
        } else {
            if (!empty($book_id)) {
                $upload = $ci->upload->data();
                $pdf_full = $uploadDir.$upload["client_name"];
                if($is_paid == true && $pdf_start > 0){
                    $ci->load->library("Fpdf_gen","fpdf_gen");
                    $arrpdffree = $this->fpdf_gen->concat($pdf_full, $pdf_start);
                    if(isset($arrpdffree["filename"]) && !empty($arrpdffree["filename"])){
                        $cover_url["pdf_free"] = $arrpdffree["filename"];
                    }else{
                        $ci->response(array('code' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, 'message' => $arrpdffree["message"], 'data' => (object)array()), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }
                $where_id = array("book_id" => $book_id);
                $cover_url = array();
                $assetscover = $pdf_full;
                $cover_url["pdf_url"] = $assetscover;
                $update = $this->db->update("book", $cover_url, $where_id);
                if ($update) {
                    $resps = array('code' => REST_Controller::HTTP_OK, 'message'=> 'sukses', 'pdf_url' => $cover_url["pdf_url"], 'pdf_free' => isset($cover_url["pdf_free"]) ? $cover_url["pdf_free"] : "");
                } else {
                    $resps = array("code" => REST_Controller::HTTP_INTERNAL_SERVER_ERROR, "message" => "Error Input to DB Image");
                }
            } else {
                $resps = array("code" => REST_Controller::HTTP_NOT_FOUND, "message" => "pdf not found");
            }
        }
        return $resps;
    }
}
if (!function_exists("pdf_s3upload")) {

    function pdf_s3upload($tmpName, $filen='')
    {
        $ci =& get_instance();
        if(!class_exists("S3")) $ci->load->library("S3", "s3");
        $bucket = REST_Controller::bucket_staging;
        if (strpos(base_url(), REST_Controller::api_prod) !== false) $bucket = REST_Controller::bucket_prod;
        if (!function_exists("sethead")) $ci->load->helper("baboo");
        if (!empty($tmpName)) {
            if(empty($filen)) $filen = basename($tmpName);
            $fileName = "baboo-docs/" . $filen;
            $stats = false;
            if ($ci->s3->upload($fileName, $tmpName, $bucket)) {
                if (@unlink(realpath($tmpName))) {
                    $stats = true;
                }
            }
            $assetOutput = $ci->s3->url($fileName, $bucket);
            if (!empty($assetOutput)) return $assetOutput;
            else return "";
        } else {
            return "";
        }
    }
}

if (!function_exists("pdf_move")) {

    function pdf_move($tmpName, $filen='')
    {
        if (!empty($tmpName)) {
            if(empty($filen)) $filen = basename($tmpName);
            $fileName = "baboo-docs/" . $filen;
            $stats = false;
            if (copy($tmpName, '../../uploads/'.$fileName)) {
                if (@unlink(realpath($tmpName))) {
                    $stats = true;
                }
            }
            $assets = (strpos(base_url(), REST_Controller::api_dev) !== false) ? REST_Controller::assets_dev : "http://localhost/uploads/";
            $assetOutput = $assets.$fileName;
            if (!empty($assetOutput)) return $assetOutput;
            else return "";
        } else {
            return "";
        }
    }
}